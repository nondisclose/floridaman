from optional import Optional

TESTS = [ 
        lambda x : _test_optional_of_with_value(),
        lambda x : _test_optional_of_without_value(),
        lambda x : _test_optional_empty() ]

def _test_optional_of_with_value():
    val = 1
    opt = Optional.of(val)
    return opt.get() == val

def _test_optional_of_without_value():
    val = 1
    try:
        opt = Optional.of(None)
    except:
        return True
    return False

def _test_optional_empty():
    opt = Optional.empty()
    try:
        opt.get()
    except:
        return True
    return False

assert all(TESTS) == True
