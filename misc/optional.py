class Optional:
    def __init__(self, v):
        self.v = v

    def is_empty(self):
        return self.v == None

    def is_present(self):
        return not self.is_empty()

    def get(self):
        if self.is_present():
            return self.v
        raise Exception('Optional#get is empty. Guard call with Optional#is_present')

    def if_present(self, fn):
        if self.is_present():
            fn(self.v)

    def if_present_or_else(self, presentFn, emptyFn):
        if self.is_present():
            return presentFn(self.v)
        else:
            return emptyFn()

    def map(self, fn):
        if self.is_present():
            return fn(self.v)

    @staticmethod
    def of(value):
        if value:
            return Optional(value)
        raise Exception('Optional#of cannot be called with null value')

    @staticmethod
    def empty():
        return Optional(None)

    @staticmethod
    def ofNullable(value):
        return Optional.of(value) if value else Optional.empty()

